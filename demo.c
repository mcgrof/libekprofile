#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <limits.h>

#include "profile.h"
#include "prof_err.h"

static const char *config_fn[] = { "demo-0001.cfg", 0 };

#define DEMO_MAX_KEY		1024
#define DEMO_MAX_VALUE		PATH_MAX
#define DEMO_MAX_BUFFER		DEMO_MAX_KEY + DEMO_MAX_VALUE + 3

static int demo_check_file(const char *filename,
			   const struct stat *sb,
			   unsigned long long max_entries)
{
	unsigned long long size_limit;

	size_limit = DEMO_MAX_BUFFER * max_entries;

	printf("-------- Limits --------------\n");
	/*
	 * Detect wrap around -- if max_entries * size_limit goves over
	 * unsigned long long we detect tha there.
	 */
	if (size_limit < max_entries) {
		fprintf(stderr,
			"Cannot do sanity check as limit is beyond limit\n");
		return -E2BIG;
	}

	if (max_entries > INT_MAX) {
		printf("Must be doing a stress test -- limit is beyond what libini_config undersands\n");
	}

	printf("Max entries allowed %llu\n", max_entries);
	printf("File size limit: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB\n",
	       size_limit,
	       size_limit / 1024,
	       size_limit / 1024 / 1024,
	       size_limit / 1024 / 1024 / 1024);

	printf("File size: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB)\n",
	       (unsigned long long) sb->st_size,
	       (unsigned long long) sb->st_size / 1024,
	       (unsigned long long) sb->st_size / 1024 / 1024,
	       (unsigned long long) sb->st_size / 1024 / 1024 / 1024);

	if (sb->st_size > size_limit) {
		fprintf(stderr,
			"File %s is too big! Max entries expected: %llu\n",
			filename, max_entries);
		return -E2BIG;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	profile_t profile;
	struct stat _sp;
	struct stat *sp = &_sp;
	int ret;
	unsigned long long max_entries = 1000;
	const char *names[4];
	char **sections = NULL;
	char **section = NULL;
	char **attrs = NULL;
	char **attr= NULL;
	unsigned int i = 0, x= 0;
	int size = 0, num_attrs;
	unsigned int val_old;

	initialize_prof_error_table();

	names[0] = NULL;
	names[1] = NULL;
	names[2] = NULL;
	names[3] = 0;

	ret = profile_init(config_fn, &profile);
	if (ret) {
		fprintf(stderr, "profile_init() failed: %s\n", error_message(ret));
		return ret;
	}

	ret = profile_get_stat(profile, sp);
	if (!sp) {
		ret = -EACCES;
		fprintf(stderr, "profile_get_stat(): could get stat on file: %s: %s\n",
			config_fn[0], strerror(ret));
		goto out;
	}

	printf("Your system limits.h says:\n\n");
	printf("INT_MAX: %u\n", INT_MAX);
	printf("UINT_MAX: %u\n", UINT_MAX);
	printf("LONG_MAX: %lu\n", LONG_MAX);
	printf("ULLONG_MAX: %llu\n", ULLONG_MAX);
	printf("\n");

	ret = demo_check_file(config_fn[0], sp, max_entries);
	if (ret) {
		fprintf(stderr, "demo_check_file() failed\n");
		goto out;
	}

	printf("demo_check_file() OK!\n");

	ret = profile_get_subsection_names(profile, names, &sections);
	if (ret || sections == 0) {
		fprintf(stderr, "profile_get_subsection_names() failed: %s\n",
			error_message(ret));
		goto out;
	}

	for (section = sections; *section; section++)
		size++;

	section = sections;

	/*
	 * We need to traverse backwards if we want to print out in the
	 * same order as in the file. Its just the way the parser works.
	 */
	section = sections + size - 1;

	printf("Number of sections: %d\n", size);
	printf("Parsing file:\n\n");
	printf("----------------------------------------------------------\n");

	/* The *section test is not needed here */
	for (i=0; i < size && *section; i++) {
		printf("[%s]\n", *section);

		names[0] = *section;
		ret = profile_get_relation_names(profile, names, &attrs);
		if (ret || attrs == 0) {
			fprintf(stderr, "profile_get_subsection_names() failed: %s\n",
				error_message(ret));
			goto out;
		}
		for (attr = attrs; *attr; attr++)
			num_attrs++;

		attr = attrs + num_attrs - 1;

		for (x=0; x < num_attrs; x++) {
			/*
			 * Needed for when we're parsing last empty element.
			 * If traversing from the end this will be our first.
			 * Its just the way the parser works...
			 */
			if (*attr == 0 || !*attr) {
				attr--;
				continue;
			}
			ret = profile_get_uint(profile, *section, *attr, 0, 0, &val_old);
			if (ret) {
				fprintf(stderr, "profile_get_subsection_names() failed: %s\n",
					error_message(ret));
				goto out;
			}
			printf("%s = %u\n", *attr, val_old);
			attr--;
		}
		section--;
	}

out:
	if (profile)
		profile_release(profile);
	return ret;
}
