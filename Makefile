all: demo

CCFLAGS := -Wall -DPROFILE_USE_CONFIG=1 -std=gnu99 -Werror -pedantic

PROFILE_C := profile.c prof_err.c error_message.c et_name.c profile_helpers.c

demo: demo.c demo-0001.cfg
	gcc $(LDFLAGS) $(CCFLAGS) -o demo demo.c $(PROFILE_C)

clean:
	rm -f demo *.o

dist-clean:
	rm -f config.* configure
	rm -rf autom4te.cache
