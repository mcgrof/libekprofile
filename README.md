libekprofile
============

Upstream e2fsprogs carries its own light weight MIT Kerberos V5 profile
parser port. Its feels like it has its own kitchen sink, but in practice
it also means less code.

    $ size -t \
            lib/support/profile.o \
            lib/support/profile_helpers.o \
            lib/support/prof_err.o \
            lib/et/error_message.o \
            lib/et/et_name.o
       text    data     bss     dec     hex filename
       8842       0       8    8850    2292 lib/support/profile.o
       2010       0       0    2010     7da lib/support/profile_helpers.o
       1582       0      16    1598     63e lib/support/prof_err.o
       2158      16     121    2295     8f7 lib/et/error_message.o
        179       0       6     185      b9 lib/et/et_name.o
      14771      16     151   14938    3a5a (TOTALS)

    $ size  \
           /usr/lib64/libconfig.so.8.3.1 \
           /usr/lib64/libini_config.so.5.2.0
       text    data     bss     dec     hex filename
      46084    1376       8   47468    b96c /usr/lib64/libconfig.so.8.3.1
     100462    2256       8  102726   19146 /usr/lib64/libini_config.so.5.2.0

The e2fsprogs profile parser is 14.86% the size libini_config.so.5.2.0!
Worth giving it a spin if you are considering a leight weight file
parser library.

Build
=====

./autogen.sh
./configure
make

Run
===

./demo

Other libraries
===============

Other libraries reviewed:

  o libconfig: https://gitlab.com/mcgrof/libconfig-int64-issues
  o libini_config: https://gitlab.com/mcgrof/libini_config-demo

libconfig has a series of issues when supporting and parsing 64-bit integer
values. The above repository documents those issues. libini_config is much
better, it is however 85.14% the size of the e2fsprogs profile parser.

For this reason the profile parser has been taken out from e2fsprogs, and
its been further extended with a few minor tweaks to test it and slightly
enhance it.

Future work
===========

If we're going to be using a file configuration parser for different filesystem
utilities do we want to share a leight weight one? Discuss in the community.
